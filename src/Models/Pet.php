<?php

namespace App\Models;

use App\Interfaces\PetsInterface;

/**
 * Class Pet.
 */
abstract class Pet implements PetsInterface
{
    abstract public function getName(): string;

    abstract public function sit();
}