<?php

namespace App\Models;

use App\Interfaces\PetOwnerInterface;
use App\Interfaces\PetsInterface;

/**
 * Class Person who owns Pets.
 */
class Person implements PetOwnerInterface
{
    /**
     * @var PetsInterface[]
     */
    private $pets = [];

    /**
     * Return Pets of owned this person
     * @return PetsInterface[]
     */
    public function getMyPets(): array
    {
        return $this->pets;
    }

    /**
     * Put Pets to owned this person
     * @param PetsInterface $pet
     */
    public function appendPet(PetsInterface $pet)
    {
        $this->pets[] = $pet;
    }
}