<?php

namespace App\Models;

use App\Interfaces\NoiseInterface;

/**
 * Class Cat.
 * Cat is Pet. Cat make noise, sit and has Name.
 */
class Cat extends Pet implements NoiseInterface
{
    /**
     * @var bool
     */
    private $isSit = false;
    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name this is Name of Cat
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function sit()
    {
        $this->isSit = true;
    }

    public function makeNoise()
    {
        echo 'Meow';
    }
}