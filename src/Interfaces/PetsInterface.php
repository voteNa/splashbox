<?php

namespace App\Interfaces;

interface PetsInterface
{
    public function getName(): string;
    public function sit();
}