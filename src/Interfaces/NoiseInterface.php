<?php

namespace App\Interfaces;

interface NoiseInterface
{
    public function makeNoise();
}