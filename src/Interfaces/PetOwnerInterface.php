<?php

namespace App\Interfaces;

interface PetOwnerInterface
{
    /**
     * Return Pets of owned this person
     * @return PetsInterface[]
     */
    public function getMyPets(): array;

    /**
     * Put Pets to owned this person
     */
    public function appendPet(PetsInterface $pet);
}