<?php

use App\Interfaces\NoiseInterface;
use App\Models\Cat;
use App\Models\Dog;
use App\Models\Person;

require __DIR__ . '/vendor/autoload.php';

$person = new Person();
$person->appendPet(new Cat('Leopold'));
$person->appendPet(new Dog('Sharik'));

foreach ($person->getMyPets() as $pet) {
    echo $pet->getName() . ': ';
    if ($pet instanceof NoiseInterface) {

        $pet->makeNoise();
    }
    echo PHP_EOL;
}