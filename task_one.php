<?php
/**
 * To test this app run: php task_one.php [n]
 * n - count columns of matrix
 */
$n = $argv[1] ?? 5;
if (empty($input)) {
    for ($i = 0; $i < 35; $i++) {
        $input[] = 1 === rand(0, 1);
    }
}

foreach (toMatrix($input, $n) as $element) {
    echo $element;
}

/**
 * The function splits a linear array into the required count of columns
 * returns items to display.
 * @param array $input
 * @param int $columnCount
 * @return Generator
 */
function toMatrix(array $input, int $columnCount): Generator
{
    if ($columnCount < 1) {
        return;
    }

    $key = 0;
    $isEndLine = false;
    while (isset($input[$key]) || false === $isEndLine) {
        yield (int)($input[$key] ?? 0);
        $isEndLine = 0 === ($key + 1) % $columnCount;
        if (true === $isEndLine) {
            yield PHP_EOL;
        }
        $key++;
    }
}
